﻿using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Threading.Tasks;
using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.TestUtilities;
using Dapper;
using Npgsql;
using TicketManagement.Domain.Entities;
using TicketMangement.Api.Functions;
using TicketMangement.Api.Requests;
using TicketMangement.Infrastructure.DataAccess;
using TicketMangement.IntegrationTests.Core;
using Xunit;

namespace TicketMangement.IntegrationTests.TicketMangement.Api
{
    [Collection(nameof(IntegrationCollection))]
    public class UserFunctionsTests : TestBase
    {
        public UserFunctionsTests(IntegrationFixture fixture)
            : base(fixture)
        {
        }


        [Fact]
        public async Task Test1()
        {
            using var connection = new NpgsqlConnection(PostgresConnectionString);
            Console.WriteLine(PostgresConnectionString);

            //PostgresDbSeeder.Seed(PostgresConnectionString).Wait();

            using (var conn = new NpgsqlConnection(PostgresConnectionString))
            {
                conn.Open();
                var query = "SELECT version()";
                var query1 = "CREATE SCHEMA data";
                var query2 = @"CREATE TABLE data.TestTable(Id SERIAL PRIMARY KEY, FirstName CHARACTER VARYING(30), LastName CHARACTER VARYING(30))";

                var query4 = @"insert into data.TestTable (FirstName, LastName) values ('Alec', 'Li'), ('Bill', 'Gace')";

                var query3 = @"select * from data.TestTable";

                var result = await connection.QueryFirstAsync<string>(query);

                await connection.QueryAsync(query1);
                await connection.QueryAsync(query2);
                await connection.QueryAsync(query4);


                var result1 = await connection.QueryFirstAsync<TestUser>(query3);




                Console.WriteLine(JsonSerializer.Serialize(result));
                Console.WriteLine(JsonSerializer.Serialize(result1));
            }

            Assert.False(string.IsNullOrWhiteSpace(PostgresConnectionString));
        }

        [Fact]
        public void GetAllUsers()
        {
            // Arrange
            var userFunction = new UserFunctions(new UserRepository());
            var request = new APIGatewayProxyRequest
            {
                Body = JsonSerializer.Serialize(new GetAllUsersRequest
                {
                    UserDomenZone = "Zone A"
                })
            };
            var context = new TestLambdaContext();
            var expectedResponseBody = JsonSerializer.Serialize(new List<User>
            {
                new User
                {
                    FirstName = "Alex",
                    LastName = "Li",
                },
                new User
                {
                    FirstName = "Elon",
                    LastName = "Mask",
                },
            });

            // Act
            var response = userFunction.GetAllUsers(request, context);

            // Assert
            Assert.Equal(200, response.StatusCode);
            Assert.Equal(expectedResponseBody, response.Body);
        }
    }

    public class TestUser
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}
