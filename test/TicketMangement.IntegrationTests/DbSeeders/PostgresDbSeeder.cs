﻿using System;
using System.Threading.Tasks;
using Npgsql;

namespace TicketMangement.IntegrationTests.DbSeeders
{
    public class PostgresDbSeeder
    {
        public static async Task Seed(string connectionString)
        {
            var createSchemaQuery = "CREATE SCHEMA data";
            var createTestTable = @"CREATE TABLE data.TestTable(Id SERIAL PRIMARY KEY, FirstName CHARACTER VARYING(30), LastName CHARACTER VARYING(30))";

            using (var connection = new NpgsqlConnection(connectionString))
            using (var createSchemaCommand = new NpgsqlCommand(createSchemaQuery, connection))
            using (var createTestTableCommand = new NpgsqlCommand(createTestTable, connection))
            {
                Console.WriteLine("connection");
                await connection.OpenAsync();
                Console.WriteLine("createSchemaCommand");
                await createSchemaCommand.ExecuteNonQueryAsync();
                Console.WriteLine("createTestTableCommand");
                await createTestTableCommand.ExecuteNonQueryAsync();
            }
        }
    }
}
