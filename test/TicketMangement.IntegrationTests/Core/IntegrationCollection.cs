﻿using Xunit;

namespace TicketMangement.IntegrationTests.Core
{
    [CollectionDefinition(nameof(IntegrationCollection))]
    public class IntegrationCollection : ICollectionFixture<IntegrationFixture>
    {
    }
}
