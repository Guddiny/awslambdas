﻿using System;
using System.Threading.Tasks;
using TestEnvironment.Docker;
using TestEnvironment.Docker.Containers.Postgres;
using TicketManagement.DataBase;

namespace TicketMangement.IntegrationTests.Core
{
    public class IntegrationFixture : IDisposable
    {
        private const string PostgresContainerName = "postgres";

        public IntegrationFixture()
        {
            //Environment = PrepareDockerEnvironment().Result;
            //var postgres = Environment.GetContainer<PostgresContainer>(PostgresContainerName);

            //PostgresConnectionString = postgres.GetConnectionString();

            PostgresConnectionString = @"User ID=root;Password=123456;Host=postgres;Database=nice_marmot;";
            //var migrationMagager = new MigrationManager();
            //migrationMagager.ApplyMigrations();
        }

        public DockerEnvironment Environment { get; }

        public string PostgresConnectionString { get; }

        private static async Task<DockerEnvironment> PrepareDockerEnvironment()
        {
            var env = new DockerEnvironmentBuilder()
                .SetName("TicketMangementApiTests")
                .AddPostgresContainer(PostgresContainerName)
                .UseDefaultNetwork()
                .Build();

            Console.WriteLine("PrepareDockerEnvironment - Start");

            await env.Up();

            Console.WriteLine("PrepareDockerEnvironment - Finish");

            return env;
        }

        public void Dispose()
        {

        }
    }
}
