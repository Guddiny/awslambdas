﻿namespace TicketMangement.IntegrationTests.Core
{
    public class TestBase
    {
        public TestBase(IntegrationFixture fixture)
        {
            PostgresConnectionString = fixture.PostgresConnectionString;
        }

        public string PostgresConnectionString { get; }
    }
}
