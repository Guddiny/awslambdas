﻿using System.Collections.Generic;
using TicketManagement.Domain.Entities;

namespace TicketMangement.Application.Common.Interfaces.DataAccess
{
    public interface IUserRepository
    {
        IEnumerable<User> GetAllUsers();
    }
}
