﻿using FluentMigrator;

namespace TicketManagement.DataBase
{
    [Migration(20180430121800)]
    public class AddLogTable : Migration
    {
        public override void Down()
        {
            Delete.Table("Log");
        }

        public override void Up()
        {
            Create.Table("Log")
                 .WithColumn("Id").AsInt64().PrimaryKey().Identity()
                 .WithColumn("Text").AsString()
                 .WithColumn("IDsOd").AsInt64().ForeignKey("User", "Id");

            Create.Table("User")
                .WithColumn("Id").AsInt64().PrimaryKey().Identity()
                .WithColumn("Name").AsString();

            Execute.Sql(" ");
        }
    }
}
