﻿using System.Collections.Generic;
using TicketManagement.Domain.Entities;
using TicketMangement.Application.Common.Interfaces.DataAccess;

namespace TicketMangement.Infrastructure.DataAccess
{
    public class UserRepository : IUserRepository
    {
        public IEnumerable<User> GetAllUsers()
        {
            return new List<User>
            {
                new User
                {
                    FirstName = "Alex",
                    LastName = "Li",
                },
                new User
                {
                    FirstName = "Elon",
                    LastName = "Mask",
                },
            };
        }
    }
}
