﻿using System;
using System.Diagnostics.CodeAnalysis;
using Amazon.Lambda.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TicketMangement.Application.Common.Interfaces.DataAccess;
using TicketMangement.Infrastructure.DataAccess;

[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

namespace TicketMangement.Api
{
    [ExcludeFromCodeCoverage]
    public class Startup
    {
        private static readonly Lazy<Startup> Lazy = new Lazy<Startup>(() => new Startup());

        public Startup()
        {
            // Configuration = new ConfigurationBuilder()
            // .Build();

            var services = new ServiceCollection();
            ConfigureServices(services);

            ServiceProvider = services.BuildServiceProvider();
        }

        public static Startup Instance => Lazy.Value;

        public IConfigurationRoot Configuration { get; }

        public IServiceProvider ServiceProvider { get; }

        private void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<IUserRepository, UserRepository>();
        }
    }
}
