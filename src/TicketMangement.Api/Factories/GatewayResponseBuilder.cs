﻿using System.Collections.Generic;
using System.Net;
using System.Text.Json;
using Amazon.Lambda.APIGatewayEvents;

namespace TicketMangement.Api.Factories
{
    public class GatewayResponseBuilder<T>
    {
        private APIGatewayProxyResponse _response = new APIGatewayProxyResponse
        {
            Headers = new Dictionary<string, string>(),
        };

        public GatewayResponseBuilder<T> StatusCode(HttpStatusCode statusCode)
        {
            _response.StatusCode = (int)statusCode;
            return this;
        }

        public GatewayResponseBuilder<T> Body(T payload)
        {
            _response.Body = JsonSerializer.Serialize<T>(payload);
            return this;
        }

        public GatewayResponseBuilder<T> AddHeader(string key, string value)
        {
            _response.Headers.Add(key, value);
            return this;
        }

        public APIGatewayProxyResponse Build() => _response;
    }
}
