﻿using FluentValidation;
using TicketMangement.Api.Requests;

namespace TicketMangement.Api.Validators
{
    public class GetAllUsersRequestValidator : AbstractValidator<GetAllUsersRequest>
    {
        public GetAllUsersRequestValidator()
        {
            RuleFor(u => u.UserDomenZone).NotEmpty().Length(5, 200);
        }
    }
}
