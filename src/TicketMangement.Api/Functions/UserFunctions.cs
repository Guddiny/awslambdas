﻿using System.Text.Json;
using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using Microsoft.Extensions.DependencyInjection;
using TicketMangement.Api.Factories;
using TicketMangement.Api.Requests;
using TicketMangement.Api.Validators;
using TicketMangement.Application.Common.Interfaces.DataAccess;

namespace TicketMangement.Api.Functions
{
    public class UserFunctions : FunctionBase
    {
        private readonly IUserRepository _userRepository;

        public UserFunctions()
        {
            _userRepository = Startup.Instance.ServiceProvider.GetService<IUserRepository>();
        }

        public UserFunctions(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public APIGatewayProxyResponse GetAllUsers(APIGatewayProxyRequest request, ILambdaContext context)
        {
            context.Logger.Log("Get Request: GetAllUsers\n");

            var requestBody = JsonSerializer.Deserialize<GetAllUsersRequest>(request.Body);
            var validationResult = new GetAllUsersRequestValidator().Validate(requestBody);

            if (!validationResult.IsValid)
            {
                return BadRequest(validationResult.Errors);
            }

            var users = _userRepository.GetAllUsers();

            return Ok(users);
        }
    }
}
