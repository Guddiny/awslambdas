﻿using System.Net;
using Amazon.Lambda.APIGatewayEvents;

namespace TicketMangement.Api.Factories
{
    public class FunctionBase
    {
        public APIGatewayProxyResponse BadRequest<T>(T body)
        {
            return new GatewayResponseBuilder<T>()
            .AddHeader("Content-Type", "text/json")
            .StatusCode(HttpStatusCode.BadRequest)
            .Body(body)
            .Build();
        }

        public APIGatewayProxyResponse BadRequest()
        {
            return BadRequest(string.Empty);
        }

        public APIGatewayProxyResponse Ok<T>(T body)
        {
            return new GatewayResponseBuilder<T>()
            .AddHeader("Content-Type", "text/json")
            .StatusCode(HttpStatusCode.OK)
            .Body(body)
            .Build();
        }

        public APIGatewayProxyResponse Ok()
        {
            return Ok(string.Empty);
        }
    }
}